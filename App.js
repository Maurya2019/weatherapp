/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SearchScreen from './src/screens/SearchScreen';

const navigate = createStackNavigator({
  Search: SearchScreen,
},
  {
    initialRouteName: 'Search',
    defaultNavigationOption: {
      title: 'App',
       headerStyle: {  
            backgroundColor: '#f4511e',  
        },  
        headerTintColor: '#0ff',  
        headerTitleStyle: {  
            fontWeight: 'bold',  
        },  
    }
  }
);

const styles = StyleSheet.create({});

const AppContainer = createAppContainer(navigate);

export default class App extends React.Component {  
    render() {  
        return <AppContainer />;  
    }  
}  
