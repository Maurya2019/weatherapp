import React from 'react'
import { TextInput, View, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const SearchBar = ({zipCode, onZipChange,onSubmit}) => {
    return (
        <View style={styles.viewCpontainer}>
            <Icon name='search'
                style={styles.icon} />
            <TextInput style={styles.input}
                placeholder='Enter zip code to see the weather'
                value={zipCode}
                autoCapitalize='none'
                autoCorrect={false}
                maxLength={10}  
                onChangeText={newZipCode=>onZipChange(newZipCode)}
                onEndEditing={()=>onSubmit()}
            ></TextInput>

        </View>
    );
};
const styles = StyleSheet.create({
    viewCpontainer: {
        borderRadius: 5,
        backgroundColor: '#e5e5e5',
        marginHorizontal: 15,
        marginVertical: 15,
        flexDirection: 'row',
        height: 50,
    },
    input: {
        fontSize: 18,
        flex: 1,
        marginStart: 15,
    },
    icon: {
        fontSize: 25,
        alignSelf: 'center',
        marginStart: 15,
    }

});
export default SearchBar;